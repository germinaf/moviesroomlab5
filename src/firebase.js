// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
import { getAuth, signInWithPopup, GoogleAuthProvider } from "firebase/auth";
import { getFirestore } from "firebase/firestore"

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional

const firebaseConfig = {
    apiKey: "AIzaSyAdPLuw9jLaUNVgdU4fZy8aLgBzeFDUMcU",
    authDomain: "movies-clouds.firebaseapp.com",
    projectId: "movies-clouds",
    storageBucket: "movies-clouds.appspot.com",
    messagingSenderId: "252786611631",
    appId: "1:252786611631:web:55d07bf5e49662bd174b5a"
  };
  
// Initialize Firebase
const app = initializeApp(firebaseConfig);

const provider = new GoogleAuthProvider();
export const auth = getAuth();
export const signInWithFirebase = () => signInWithPopup(auth, provider);